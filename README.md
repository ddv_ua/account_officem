# Officem v. 3.0

Web-site (v. 2.0):  [Link](https://officem.com.ua)

Web-site (v. 3.0): [Link](https://crm.officem.com.ua)

Framework: Laravel v. 5

Usage:

1.    Bootstrap v.4
2.    jQuery v. 3.1.0
3.    Tether
4.    InputMask
5.    jQuery ColorBox

Authors: [eMiS](http://emis.com.ua/)

Current build: 0.25.5

####Info about structure of project may found by path:
[Sitemap](http://crm.officem.com.ua/site-map/)


[Deadline](http://crm.officem.com.ua/todo/)


[Login](http://crm.officem.com.ua/auth/login/)


[Register](http://crm.officem.com.ua/auth/register/)


Note (in russian):

Было допущено достаточно немало ошибок в проектировании работы с базой данных, но, в данный момент времени, ошибки закрыты.